﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Threading;
using EnglishWords.Word;
using System.Windows;
using System.Windows.Media.Imaging;

namespace EnglishWords.Demonstrator
{
    class ControlDemonstratorViewModel : LearnDemonstratorViewModel
    {
        protected Window EndOfGameView;
        protected SoundPlayer ApplausePlayer;

        protected BitmapImage markPicture;
        public BitmapImage MarkPicture
        {
            get { return markPicture; }
            protected set
            {
                markPicture = value; OnPropertyChanged("MarkPicture");
            }
        }

        protected string markSuccessWords;
        public string MarkSuccessWords
        {
            get { return markSuccessWords; }
            set { markSuccessWords = value; OnPropertyChanged("MarkSuccessWords"); }
        }

        protected string markCountWords;
        public string MarkCountWords
        {
            get { return markCountWords; }
            set { markCountWords = value; OnPropertyChanged("MarkCountWords"); }
        }

        protected int GuessedWordsCount;

        protected bool textBoxReadOnly;
        public bool TextBoxReadOnly
        {
            get { return textBoxReadOnly; }
            protected set { textBoxReadOnly = value;  OnPropertyChanged("TextBoxReadOnly"); }
        }

        public ControlDemonstratorViewModel() : base()
        {
            Description = "Control mode";
            ApplausePlayer = new SoundPlayer("Resources/applause.wav");
            TextBoxReadOnly = false;
            GuessedWordsCount = 0;
        }

        protected string textBoxBackground = "#FC7C7C";
        public string TextBoxBackground
        {
            get { return textBoxBackground; }
            protected set { textBoxBackground = value; OnPropertyChanged("TextBoxBackground"); }
        }

        protected string inputWord;
        public string InputWord
        {
            get { return inputWord; }
            set
            {
                inputWord = value;
                OnPropertyChanged("InputWord");
                if(inputWord.Equals(CurrentWord.Word, StringComparison.OrdinalIgnoreCase))
                {
                    TextBoxBackground = "#FFE8FFD7";
                    Task Success = new Task(OnSuccess);
                    GuessedWordsCount++;
                    if (CurrentWordIndex+1 == WordsCollection.Count)
                    {
                        ShowEndGameView();
                    }
                    Success.Start();
                }
                else
                {
                    TextBoxBackground = "#FC7C7C";
                }
            }
        }

        protected void ShowEndGameView()
        {
            EndOfGameView = new EndGameView(this);

            MarkCountWords = WordsCollection.Count.ToString();
            MarkSuccessWords = GuessedWordsCount.ToString();

            int percents = GuessedWordsCount * 100 / WordsCollection.Count;

            if(percents < 50)
            {
                MarkPicture = new BitmapImage(new Uri("pack://application:,,,/Resources/very_bad.png", UriKind.Absolute));
            }
            else if(percents < 80 && percents > 49)
            {
                MarkPicture = new BitmapImage(new Uri("pack://application:,,,/Resources/bad.png", UriKind.Absolute));
            }
            else if(percents < 100 && percents > 79)
            {
                MarkPicture = new BitmapImage(new Uri("pack://application:,,,/Resources/good.png", UriKind.Absolute));
            }
            else if(percents > 99)
            {
                MarkPicture = new BitmapImage(new Uri("pack://application:,,,/Resources/very_good.png", UriKind.Absolute));
            }

            EndOfGameView.ShowDialog();
            View.Close();
        }

        protected void OnSuccess()
        {
            ApplausePlayer.Play();
            TextBoxReadOnly = true;
            Thread.Sleep(750);
            TextBoxReadOnly = false;
            ForwardButtonWasClicked();
        }

        public override StructWord CurrentWord
        {
            get { return currentWord; }
            set { currentWord = value; OnPropertyChanged("CurrentWord"); InputWord = ""; }
        }

        public override void Go(ObservableCollection<StructWord> PlayWords)
        {
            WordsCollection = PlayWords;
            CurrentWord = WordsCollection[0];
            CurrentWordIndex = 0;
            RecountWordsLeft();
            GuessedWordsCount = 0;
            View = new ControlDemonstratorView(this);
            View.ShowDialog();
        }
    }
}
