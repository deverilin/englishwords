﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EnglishWords.Demonstrator
{
    /// <summary>
    /// Логика взаимодействия для LearnDemonstratorView.xaml
    /// </summary>
    public partial class LearnDemonstratorView : Window
    {
        public LearnDemonstratorView(BaseDemonstratorViewModel ViewModel)
        {
            InitializeComponent();
            DataContext = ViewModel;
        }
    }
}
