﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EnglishWords.Demonstrator
{
    /// <summary>
    /// Логика взаимодействия для EndGameView.xaml
    /// </summary>
    public partial class EndGameView : Window
    {
        public EndGameView(BaseDemonstratorViewModel ViewModel)
        {
            InitializeComponent();
            DataContext = ViewModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
