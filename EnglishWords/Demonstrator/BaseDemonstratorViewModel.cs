﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using EnglishWords.Word;

namespace EnglishWords.Demonstrator
{
    public abstract class BaseDemonstratorViewModel : BaseViewModel
    {
        protected Window View;

        protected string description = "Empty description";
        public string Description
        {
            get { return description; }
            protected set { description = value; OnPropertyChanged("Description"); }
        }

        public abstract void Go(ObservableCollection<StructWord> PlayWords);
    }
}
