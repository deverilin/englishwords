﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using EnglishWords.Word;

namespace EnglishWords.Demonstrator
{
    class LearnDemonstratorViewModel : BaseDemonstratorViewModel
    {

        protected ObservableCollection<StructWord> WordsCollection;

        protected int CurrentWordIndex;

        protected StructWord currentWord;
        public virtual StructWord CurrentWord
        {
            get { return currentWord; }
            set { currentWord = value; OnPropertyChanged("CurrentWord"); }
        }

        public ICommand BackBtnClicked
        {
            get { return new RelayCommand(o => { BackButtonWasClicked(); }); }
        }

        public ICommand ForwardBtnClicked
        {
            get { return new RelayCommand(o => { ForwardButtonWasClicked(); }); }
        }

        public ICommand PlayBtnClicked
        {
            get { return new RelayCommand(o => { PlaySound(); }); }
        }

        public LearnDemonstratorViewModel() : base()
        {
            Description = "Learn mode";
        }

        public override void Go(ObservableCollection<StructWord> PlayWords)
        {
            WordsCollection = PlayWords;
            CurrentWord = WordsCollection[0];
            CurrentWordIndex = 0;
            RecountWordsLeft();
            View = new LearnDemonstratorView(this);
            View.ShowDialog();
        }

        protected void RecountWordsLeft()
        {
            int currentWordNumber = CurrentWordIndex + 1;
            WordsLeft = currentWordNumber.ToString() + "/" + WordsCollection.Count.ToString();
        }

        protected void BackButtonWasClicked()
        {
            if(!(CurrentWordIndex-1 < 0))
            {
                --CurrentWordIndex;
                CurrentWord = WordsCollection[CurrentWordIndex];
                RecountWordsLeft();
            }
        }

        protected void ForwardButtonWasClicked()
        {
            if (!(CurrentWordIndex + 1 > WordsCollection.Count-1))
            {
                ++CurrentWordIndex;
                CurrentWord = WordsCollection[CurrentWordIndex];
                RecountWordsLeft();
            }
        }

        protected void PlaySound()
        {
            CurrentWord.Sound.Play();
        }

        protected string wordsLeft;
        public string WordsLeft
        {
            get { return wordsLeft; }
            protected set { wordsLeft = value;  OnPropertyChanged("WordsLeft"); }
        }
    }
}
