﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using EnglishWords.Category;
using System.IO;
using EnglishWords.Word;
using System.Windows.Media.Imaging;
using System.Media;

namespace EnglishWords
{
    class MainWindowViewModel : BaseViewModel
    {
        private CategoryViewModel selectedCategory;
        public CategoryViewModel SelectedCategory
        {
            get { return selectedCategory; }
            set { selectedCategory = value; OnPropertyChanged("SelectedCategory"); }
        }

        private ObservableCollection<CategoryViewModel> categories;     
        public ObservableCollection<CategoryViewModel> Categories
        {
            get { return categories; }
            set { categories = value; OnPropertyChanged("Categories"); }
        }

        public MainWindowViewModel()
        {
            categories = new ObservableCollection<CategoryViewModel>();
            ParseGamedataContent();
        }

        private ICommand categoryClicked;
        public ICommand CategoryClicked
        {
            get { return categoryClicked ??
                    (categoryClicked = new RelayCommand(
                     obj =>
                     {
                         SelectedCategory.ShowForm();
                     }));
            }
        }

        private void CategoryWasClicked()
        {
            SelectedCategory.ShowForm();
        }

        private void ParseGamedataContent()
        {
            DirectoryInfo rootDir = new DirectoryInfo(Directory.GetCurrentDirectory() + @"\gamedata");
            DirectoryInfo[] CategoriesDirs = null;

            try
            {
                CategoriesDirs = rootDir.GetDirectories();
            }
            catch
            {

            }

            foreach (DirectoryInfo categoryInfo in CategoriesDirs)
            {
                string catName = categoryInfo.Name;
                BitmapImage catPicture = new BitmapImage(new Uri(categoryInfo.FullName + @"\head_picture.jpg"));
                List<StructWord> catWords = new List<StructWord>();

                DirectoryInfo[] CategoryRoot = categoryInfo.GetDirectories();

                foreach (DirectoryInfo wordInfo in CategoryRoot)
                {
                    StructWord newWord = new StructWord();
                    newWord.Word = wordInfo.Name;

                    FileInfo[] WordFiles = wordInfo.GetFiles();

                    foreach (FileInfo file in WordFiles)
                    {
                        if(file.Name.Contains("picture"))
                        {
                            newWord.Picture = new BitmapImage(new Uri(file.FullName));
                        }
                        else if(file.Name.Contains("sound"))
                        {
                            newWord.Sound = new SoundPlayer(file.FullName);
                        }
                    }

                    catWords.Add(newWord);
                }

                Categories.Add(new CategoryViewModel(catWords, catName, catPicture));

            }
        }
    }
}
