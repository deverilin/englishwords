﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnglishWords.Word;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using EnglishWords.Demonstrator;
using System.Windows;
using System.Windows.Input;

namespace EnglishWords.Category
{
    public class CategoryViewModel : BaseViewModel
    {
        protected Window View;
       
        protected ObservableCollection<StructWord> Words;

        protected BaseDemonstratorViewModel selectedDemonstrator;
        public BaseDemonstratorViewModel SelectedDemonstrator
        {
            get { return selectedDemonstrator; }
            set { selectedDemonstrator = value; OnPropertyChanged("SelectedDemonstrator"); }
        }

        protected ObservableCollection<BaseDemonstratorViewModel> demonstrators;
        public ObservableCollection<BaseDemonstratorViewModel> Demonstrators
        {
            get { return demonstrators; }
            set { demonstrators = value; OnPropertyChanged("Demonstrators"); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            private set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        private BitmapImage picture;
        public BitmapImage Picture
        {
            get { return picture; }
            private set
            {
                picture = value;
                OnPropertyChanged("Picture");
            }
        }

        public CategoryViewModel(List<StructWord> NewWords, string NewName, BitmapImage NewImage)
        {
            Words = new ObservableCollection<StructWord>(NewWords);
            Name = NewName;
            Picture = NewImage;
            Demonstrators = new ObservableCollection<BaseDemonstratorViewModel>();
            Demonstrators.Add(new LearnDemonstratorViewModel());
            Demonstrators.Add(new ControlDemonstratorViewModel());
        }

        public ICommand DemonstratorClicked
        {
            get { return new RelayCommand(o => { DemonstratorWasClicked(); }); }
        }


        protected void DemonstratorWasClicked()
        {
            View.Close();
            SelectedDemonstrator.Go(Words);
        }

        public void ShowForm()
        {
            View = new CategoryView(this);
            View.Show();
        }

    }
}
