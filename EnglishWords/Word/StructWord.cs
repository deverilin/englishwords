﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Media;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace EnglishWords.Word
{
    public struct StructWord : INotifyPropertyChanged
    {
        private BitmapImage picture;
        public BitmapImage Picture
        {
            get { return picture; }
            set { picture = value; OnPropertyChanged("Picture"); }
        }

        private string word;
        public string Word
        {
            get { return word; }
            set { word = value; OnPropertyChanged("Word"); }
        }

        private SoundPlayer sound;
        public SoundPlayer Sound
        {
            get { return sound; }
            set { sound = value; OnPropertyChanged("Sound"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
